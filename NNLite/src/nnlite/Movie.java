/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nnlite;

/**
 *
 * @author Archangel V01
 */
public class Movie 
{
    private String id, title, director, genre1 = "", genre2 = "", genre3 = "";
    private int year, numRatings, runTime;
    private float rating, userRating;
    
    public Movie()
    {
        
    }
    
    public void setID(String i)
    {
        id = i;
    }
    
    public void setRating(float i)
    {
        rating = i;
    }
    
    public void setNumRatings(int i)
    {
        numRatings = i;
    }
    
    public void setTitle(String s)
    {
        title = s;
    }
    
    public void setDirector(String s)
    {
        director = s;
    }
    
    public void setRuntime(int x)
    {
        runTime = x;
    }
    
    public void setGenres(String s1, String s2, String s3)
    {
        genre1 = s1;
        if(s2 != null)
        {
            genre2 = s2;
            if(s3 != null)
            {
                genre3 = s3;
            }
        }
    }
    
    public void setYear(int x)
    {
        year = x;
    }
    
    public String getID()
    {
        return id;
    }
    
    public float getRating()
    {
        return rating;
    }
    
    public void setUserRating(float f)
    {
        userRating = f;
    }
    
    public float getUserRating()
    {
        return userRating;
        
    }
    
    public int getNumRatings()
    {
        return numRatings;
    }
    
    public String getTitle()
    {
        return title;
    }
    
    public String getGenre1()
    {
        return genre1;
    }
    
    public String getGenre2()
    {
        return genre2;
    }
    
    public String getGenre3()
    {
        return genre3;
    }
    
    public String getDirector()
    {
        return director;
    }
}

