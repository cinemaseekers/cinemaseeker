/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nnlite;

/**
 *
 * @author Archangel V01
 */
import nnlite.Movie;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jtbum
 */
public class Cinema_Seeker {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        ArrayList<Movie> movies = new ArrayList<Movie>();
        ArrayList<Movie> userMovies = new ArrayList<Movie>();
        
        BufferedReader reader = null;
        
         try {
            reader = new BufferedReader(new FileReader("./src/Cinema_Seeker/FINAL_MOVIE_LIST2.csv"));
        } catch (FileNotFoundException ex) {
             System.out.println("Invalid file! Exiting Program!");
        }
        
        boolean readingLines = true;
        String line = null;
        String[] splitLine;
        
        try {
            reader.readLine();
        } catch (IOException ex) {
            Logger.getLogger(Cinema_Seeker.class.getName()).log(Level.SEVERE, null, ex);
        }
        int count = 0;
        while(readingLines == true)         //goes through all lines of file
        {
            count++;
            //System.out.println(count);
            try {
            line = reader.readLine();
            } catch (IOException ex) {
            }

            if(line == null)
            {
                readingLines = false;
                break;
            }
            splitLine = line.split(",");    //splits file values by commas
            Movie m = new Movie();
            
            m.setID(splitLine[0]);
            
             try{
                m.setRating(Float.valueOf(splitLine[2]));
            }catch(NumberFormatException e)
            {
                System.out.println("RATING ERROR AT MOVIE " + m.getID());
            }  
            
            m.setNumRatings(Integer.valueOf(splitLine[3]));
            
            m.setTitle(splitLine[4]);
            
            try{
                m.setYear(Integer.valueOf(splitLine[5]));
            }catch(NumberFormatException e)
            {
                System.out.println("YEAR ERROR AT MOVIE " + m.getTitle());
            }  
            
            try{
                m.setRuntime(Integer.valueOf(splitLine[6]));
            }catch(NumberFormatException e)
            {
                System.out.println("RUNTIME ERROR AT MOVIE " + m.getTitle());
            }
            
            
            
            if(splitLine[7].charAt(0) != '"')
            {
                m.setGenres(splitLine[7], null, null);
                m.setDirector(splitLine[8]);
            }
            else if(splitLine[7].charAt(0) == '"' && splitLine[8].charAt(splitLine[8].length() - 1) == '"')
            {
                m.setGenres(splitLine[7].substring(1, splitLine[7].length()), splitLine[8].substring(0, splitLine[8].length() - 1), null);
                m.setDirector(splitLine[9]);
            }
            else
            {
                m.setGenres(splitLine[7].substring(1, splitLine[7].length()), splitLine[8], splitLine[9].substring(0, splitLine[9].length() - 1));
                m.setDirector(splitLine[10]);
            }
           
            movies.add(m);            
        }
        
        System.out.println("MOVIES SIZE: " + movies.size());
        String fgenre1 = "", fgenre2 = "", fgenre3 = "";
        String fdirector = "";
        int fDecade = 0, fRuntime = 0;
        
        try {
            reader = new BufferedReader(new FileReader("./src/Cinema_Seeker/ratings.csv"));
        } catch (FileNotFoundException ex) {
             System.out.println("Invalid file! Exiting Program!");
        }
        
        readingLines = true;
        
        try {
            reader.readLine();
        } catch (IOException ex) {
            Logger.getLogger(Cinema_Seeker.class.getName()).log(Level.SEVERE, null, ex);
        }
        ArrayList<String> seenIDs = new ArrayList<String>();
        ArrayList<Float> correlatedRatings = new ArrayList<Float>();
        
        while(readingLines == true)
        {
            try {
                line = reader.readLine();
            } catch (IOException ex) {
                Logger.getLogger(Cinema_Seeker.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            if(line == null)
            {
                readingLines = false;
                break;
            }
            
            splitLine = line.split(",");
            
            seenIDs.add(splitLine[0]);
            correlatedRatings.add(Float.valueOf(splitLine[1]));
            
        }
        
        for(int i = 0; i < seenIDs.size(); i++)
        {
            for(int j = 0; j < movies.size(); j++)
            {
                if(seenIDs.get(i).equals(movies.get(j).getID()))
                {
                    movies.get(j).setUserRating(correlatedRatings.get(i));
                    userMovies.add(movies.get(j));
                    movies.remove(j);
                }
            }
        }
        
        System.out.println("DONE");
        System.out.println("SEEN MOVIES SIZE: " + userMovies.size());
        System.out.println("MOVIES SIZE: " + movies.size());
    }
}

