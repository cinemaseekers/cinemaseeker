/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cinema_seeker;

/**
 *
 * @author Archangel V01
 */
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jtbum
 */
public class Cinema_Seeker {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        ArrayList<Movie> movies = new ArrayList<Movie>();
        ArrayList<Movie> userMovies = new ArrayList<Movie>();
        
        BufferedReader reader = null;
        
         try {
            reader = new BufferedReader(new FileReader("./src/Cinema_Seeker/FINAL_MOVIE_LIST2.csv"));
        } catch (FileNotFoundException ex) {
             System.out.println("Invalid file! Exiting Program!");
        }
        
        boolean readingLines = true;
        String line = null;
        String[] splitLine;
        
        try {
            reader.readLine();
        } catch (IOException ex) {
            Logger.getLogger(Cinema_Seeker.class.getName()).log(Level.SEVERE, null, ex);
        }
        int count = 0;
        while(readingLines == true)         //goes through all lines of file
        {
            count++;
            //System.out.println(count);
            try {
            line = reader.readLine();
            } catch (IOException ex) {
            }

            if(line == null)
            {
                readingLines = false;
                break;
            }
            splitLine = line.split(",");    //splits file values by commas
            Movie m = new Movie();
            
            m.setID(splitLine[0]);
            
             try{
                m.setRating(Float.valueOf(splitLine[2]));
            }catch(NumberFormatException e)
            {
                System.out.println("RATING ERROR AT MOVIE " + m.getID());
            }  
            
            m.setNumRatings(Integer.valueOf(splitLine[3]));
            
            m.setTitle(splitLine[4]);
            
            try{
                m.setYear(Integer.valueOf(splitLine[5]));
            }catch(NumberFormatException e)
            {
                System.out.println("YEAR ERROR AT MOVIE " + m.getTitle());
            }  
            
            try{
                m.setRuntime(Integer.valueOf(splitLine[6]));
            }catch(NumberFormatException e)
            {
                System.out.println("RUNTIME ERROR AT MOVIE " + m.getTitle());
            }
            
            
            
            if(splitLine[7].charAt(0) != '"')
            {
                m.setGenres(splitLine[7], null, null);
                m.setDirector(splitLine[8]);
            }
            else if(splitLine[7].charAt(0) == '"' && splitLine[8].charAt(splitLine[8].length() - 1) == '"')
            {
                m.setGenres(splitLine[7].substring(1, splitLine[7].length()), splitLine[8].substring(0, splitLine[8].length() - 1), null);
                m.setDirector(splitLine[9]);
            }
            else
            {
                m.setGenres(splitLine[7].substring(1, splitLine[7].length()), splitLine[8], splitLine[9].substring(0, splitLine[9].length() - 1));
                m.setDirector(splitLine[10]);
            }
           
            movies.add(m);            
        }
        
        System.out.println("MOVIES SIZE: " + movies.size());
        String fgenre1 = "", fgenre2 = "", fgenre3 = "";
        String fdirector = "";
        int fDecade = 0, fRuntime = 0;
        
        try {
            reader = new BufferedReader(new FileReader("./src/Cinema_Seeker/ratings.csv"));
        } catch (FileNotFoundException ex) {
             System.out.println("Invalid file! Exiting Program!");
        }
        
        readingLines = true;
        
        try {
            reader.readLine();
        } catch (IOException ex) {
            Logger.getLogger(Cinema_Seeker.class.getName()).log(Level.SEVERE, null, ex);
        }
        ArrayList<String> seenIDs = new ArrayList<String>();
        ArrayList<Float> correlatedRatings = new ArrayList<Float>();
        
        while(readingLines == true)
        {
            try {
                line = reader.readLine();
            } catch (IOException ex) {
                Logger.getLogger(Cinema_Seeker.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            if(line == null)
            {
                readingLines = false;
                break;
            }
            
            splitLine = line.split(",");
            
            seenIDs.add(splitLine[0]);
            correlatedRatings.add(Float.valueOf(splitLine[1]));
            
        }
        
        for(int i = 0; i < seenIDs.size(); i++)
        {
            for(int j = 0; j < movies.size(); j++)
            {
                if(seenIDs.get(i).equals(movies.get(j).getID()))
                {
                    movies.get(j).setUserRating(correlatedRatings.get(i));
                    userMovies.add(movies.get(j));
                    movies.remove(j);
                }
            }
        }
        Criteria c = new Criteria();
        ArrayList<Chromosome> nextGen = new ArrayList<Chromosome>();
        ArrayList<Chromosome> parentPool = new ArrayList<Chromosome>();
        
        c.getGenreAverages(userMovies);
        c.scoreDirectors(userMovies);
        
        System.out.println("DONE");
        System.out.println("SEEN MOVIES SIZE: " + userMovies.size());
        System.out.println("MOVIES SIZE: " + movies.size());
        
        int popSize = 1000;
        int gen = 1000;
        double crossRate = 0.4;
        double mutRate = 0.05;
        int bestGen = 0;
        int highScore = 0;
        
        ArrayList<Chromosome> population = new ArrayList<Chromosome>();
        Random rand = new Random();
        
        for(int i = 0; i < popSize; i++)
        {
            Chromosome chro = new Chromosome();
            chro.movies.add(movies.get(rand.nextInt(movies.size())));
            chro.movies.add(movies.get(rand.nextInt(movies.size())));
            chro.movies.add(movies.get(rand.nextInt(movies.size())));
            chro.movies.add(movies.get(rand.nextInt(movies.size())));
            chro.movies.add(movies.get(rand.nextInt(movies.size())));
            chro.scoreList(c);
            
            population.add(chro);
        }
        int Parent1;
        int Parent2;
        for(int g = 0; g < gen; g++)
        {
            /*
            for(int i = 0; i < popSize; i++)
            {
                System.out.println("SCORE: " + population.get(i).getScore());
                for(int j = 0; j < 5; j++)
                {
                    System.out.println("\t" + population.get(i).movies.get(j).getTitle());
                }
            }
            */

            for (int i = 0; i < popSize; i++)
            {
                population.get(i).scoreList(c);
                //System.out.println(population.get(i).getScore());
                if(highScore < population.get(i).getScore())
                {
                    highScore = population.get(i).getScore();
                    bestGen = g + 1;
                    System.out.println("NEW HIGH SCORE AT GEN " + (g + 1) + " WITH HIGH SCORE: " + highScore);
                    population.get(i).displayChromosome();
                }
            }
            
            //bubble sort
            for (int i = 0; i < popSize; i++)
            {
                for (int j = 0; j < popSize - i - 1; j++)
                if (population.get(j).getScore() > population.get(j + 1).getScore())
                {
                    Collections.swap(population, j, j + 1);
                }
            }
        
            System.out.println("GENERATION " + (g + 1));
            parentPool.clear();
            nextGen.clear();
            
            //Selection: Elitism 50%
            for(int i = popSize - 1; i > popSize/2; i--)
            {
                //System.out.println("ADDED CHROMOSOME " + i + " WITH FITNESS " + chromosomes.get(i).getFitness());
                parentPool.add(population.get(i));
                nextGen.add(population.get(i));
            }
            
            //crossover
            Movie temp1 = new Movie();
            Movie temp2 = new Movie();
            
            for(int i=0; i < parentPool.size()-1;i++)
            {
                Parent1 = rand.nextInt(parentPool.size());
                Parent2 = rand.nextInt(parentPool.size());
                while(Parent2 == Parent1){
                    Parent2 = rand.nextInt(parentPool.size());
                }
                Chromosome child1 = new Chromosome();
                child1.movies = parentPool.get(Parent1).movies;
                
                Chromosome child2 = new Chromosome();
                child2.movies = parentPool.get(Parent2).movies;
                
                temp1 = child1.movies.remove(0);
                temp2 = child2.movies.remove(0); 
                child1.movies.add(temp2);
                child2.movies.add(temp1);
                
                if(crossRate >= 0.4)
                {
                    temp1 = child1.movies.remove(1);
                    temp2 = child2.movies.remove(1); 
                    child1.movies.add(temp2);
                    child2.movies.add(temp1);
                }
                if(crossRate >= 0.6)
                {
                    temp1 = child1.movies.remove(2);
                    temp2 = child2.movies.remove(2); 
                    child1.movies.add(temp2);
                    child2.movies.add(temp1);
                }
                if(crossRate >= 0.8)
                {
                    temp1 = child1.movies.remove(3);
                    temp2 = child2.movies.remove(3); 
                    child1.movies.add(temp2);
                    child2.movies.add(temp1);
                }
                if(crossRate >= 1.0)
                {
                    temp1 = child1.movies.remove(4);
                    temp2 = child2.movies.remove(4); 
                    child1.movies.add(temp2);
                    child2.movies.add(temp1);
                }
                
                if(rand.nextDouble() >= mutRate)
                {
                    child1.movies.remove(rand.nextInt(4));
                    child1.movies.add(movies.get(rand.nextInt(movies.size())));
                }
                if(rand.nextDouble() >= mutRate)
                {
                    child2.movies.remove(rand.nextInt(4));
                    child2.movies.add(movies.get(rand.nextInt(movies.size())));
                }
                nextGen.add(child1);
                nextGen.add(child2);
            }
            if(nextGen.size() < popSize)
            {
                nextGen.add(population.get(rand.nextInt(popSize / 2)));
            }
            
        }
        System.out.println("BEST SCORE WAS " + highScore + " AT GENERATION " + bestGen);
    }
}

