/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cinema_seeker;

import java.util.ArrayList;

/**
 *
 * @author jtbum
 */
public class Chromosome 
{
    private int score = 0;
    public ArrayList<Movie> movies = new ArrayList<Movie>();
    
    public Chromosome()
    {
        
    }
    
    public void displayChromosome()
    {
        for(int i = 0; i < movies.size(); i++)
        {
           System.out.println(movies.get(i).getRating() + "\t" + movies.get(i).getTitle() + "\tDIRECTED BY " + movies.get(i).getDirector() + "\tGENRES: " + movies.get(i).getGenres()); 
        }
    }
    
    public int getScore()
    {
        return score;
    }
    
    
    public void scoreList(Criteria c)
    {
        score = 0;
        int temp = 0;
        for(int i = 0; i < movies.size(); i++)
        {
            for(int j = 0; j < c.genres.size(); j++)
            {
                if(movies.get(i).getGenre1().equals(c.genres.get(j)))
                {
                    temp += c.genreScores.get(j);
                }
                if(movies.get(i).getGenre2().equals(c.genres.get(j)))
                {
                    temp += c.genreScores.get(j);
                }
                if(movies.get(i).getGenre3().equals(c.genres.get(j)))
                {
                    temp += c.genreScores.get(j);
                }
                
                if(movies.get(i).getGenre2().equals(null))
                {
                    score += temp;
                }
                else if(movies.get(i).getGenre3().equals(null))
                {
                    score += temp / 2;
                }
                else
                {
                    score += temp / 3;
                }
                
                temp = 0;
            }
            
            for(int j = 0; j < c.directors.size(); j++)
            {
                if(movies.get(i).getDirector().equals(c.directors))
                {
                    score += c.directorScores.get(j);
                }
            }
        }
        
        for(int i = 0; i < movies.size(); i++)
        {
            for(int j = i + 1; j < movies.size(); j++)
            {
                if(movies.get(i).getID().equals(movies.get(j).getID()))
                {
                    score -= 1500;
                }
            }
        }
        
        for(int i = 0; i < movies.size(); i++)
        {
            if(movies.get(i).getRating() < 5.0)
            {
                score -= 500;
            }
        }
        
    }
}
