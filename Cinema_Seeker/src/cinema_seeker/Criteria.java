/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cinema_seeker;

import java.util.ArrayList;

/**
 *
 * @author jtbum
 */
public class Criteria 
{
    //27
    /*
    Action
Adventure
Animation
Biography
Comedy
Crime
Documentary
Drama
Family
Fantasy
Film Noir
History
Horror
Music
Musical
Mystery
Romance
Sci-Fi
Short
Sport
Superhero
Thriller
War
Western
    */
    public ArrayList<String> genres = new ArrayList<String>();
    public ArrayList<Integer> genreScores = new ArrayList<Integer>();
    public ArrayList<String> directors = new ArrayList<String>();
    public ArrayList<Integer> directorScores = new ArrayList<Integer>();
    
    public Criteria()
    {
        genres.add("Action");
        genres.add("Adventure");
        genres.add("Animation");
        genres.add("Biography");
        genres.add("Comedy");
        genres.add("Crime");
        genres.add("Documentary");
        genres.add("Drama");
        genres.add("Family");
        genres.add("Fantasy");
        genres.add("Film Noir");
        genres.add("History");
        genres.add("Horror");
        genres.add("Music");
        genres.add("Musical");
        genres.add("Mystery");
        genres.add("Romance");
        genres.add("Sci-Fi");
        genres.add("Short");
        genres.add("Sport");
        genres.add("Superhero");
        genres.add("Thriller");
        genres.add("War");
        genres.add("Western");
    }
    
    public void getGenreAverages(ArrayList<Movie> m)
    {
        double sum = 0;
        double average = 0;
        int count;
        
        for(int i = 0; i < genres.size(); i++)
        {
            sum = 0;
            count = 0;
            average = 0;
            
            for(int j = 0; j < m.size(); j++)
            {                
                if(m.get(j).getGenre1().equals(genres.get(i)))
                {
                    sum += m.get(j).getUserRating();
                    count++;
                }
                else if(m.get(j).getGenre2().equals(genres.get(i)))
                {
                    sum += m.get(j).getUserRating();
                    count++;
                }
                else if(m.get(j).getGenre3().equals(genres.get(i)))
                {
                    sum += m.get(j).getUserRating();
                    count++;
                }
            }
            
            average = sum / count;
            average = average * 100.0;
            genreScores.add((int)average);
        }
        
        for(int i = 0; i < genres.size(); i++)
        {
            System.out.println("GENRE: " + genres.get(i) + " AVERAGE SCORE: " + genreScores.get(i));
        }
    }
    
    public void scoreDirectors(ArrayList<Movie> m)
    {
        directors.add(m.get(0).getDirector());
        boolean add = true;
        
        for(int i = 1; i < m.size(); i++)
        {
            add = true;
            for(int j = 0; j < directors.size(); j++)
            {
                if(directors.get(j).equals(m.get(i).getDirector()))
                {
                    add = false;
                }
            }
            if(add == true)
            {
                directors.add(m.get(i).getDirector());
            }
        }
        
        int sum = 0;
        int count = 0;
        double average = 0;
        
         for(int i = 0; i < directors.size(); i++)
        {
            sum = 0;
            count = 0;
            average = 0;
            
            for(int j = 0; j < m.size(); j++)
            {                
                if(m.get(j).getDirector().equals(directors.get(i)))
                {
                    sum += m.get(j).getUserRating();
                    count++;
                }
                
            }
            
            average = (double)sum / (double)count;
            average = average * 100.0;
            if(average <= 400 && count != 0)
            {
               directorScores.add(-500);
            }
            else
            {
                directorScores.add((int)average);
            }
            
        }

        for(int i = 0; i < directors.size(); i++)
        {
            System.out.println("DIRECTOR: " + directors.get(i) + " AVERAGE SCORE: " + directorScores.get(i));
        }
    }
}
